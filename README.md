# Mastodon2Diasp

Post ATOM feeds from Mastodon to Diaspora.

This program is based on [feedDiasp*](https://github.com/Debakel/feedDiasp) by [Moritz Duchêne](https://github.com/Debakel)
and [Alexey Veretennikov](https://github.com/fourier) with some additional code to parse and handle Mastodon ATOM feeds
correctly. This includes:

* skip posts which are part of a conversation and start with a ```@```
* mark re-shared posts with "via original-author"

# Usage / Installation

* checkout this repository
* get feedDiasp: ```sudo pip install feeddiasp```
* copy the settings.sample.cfg to settings.cfg and adjust the values
* run ```python publish.py```