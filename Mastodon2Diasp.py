#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Mastodon2Diasp.py: based on feedDiasp from Moritz Duchêne and Alexey Veretennikov with some modifications to handle
#                    Mastodon's ATOM feed correctly
#
# Copyright (C) 2017  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from feeddiasp import FeedDiasp
from feeddiasp.Diasp import Diasp
from Mastodon import Mastodon

class Mastodon2Diasp(FeedDiasp):

    def __init__(self, pod, username, password, db, parser, mastodonUserName, provider, keywords=None, hashtags=None, append=None):
        FeedDiasp.__init__(self, pod, username, password, db, parser, keywords, hashtags, append)
        self.diasp = Diasp(pod=self.pod, username=self.username, password=self.password, provider_name=provider)
        if not self.diasp.logged_in:
            self.diasp.login()
        self.mastodon = Mastodon(mastodonUserName)

    def publish(self):
        """ publish post to Diaspora """
        self.feed.update()
        posts = self.feed.get_entries()
        for post in posts:
            title = post['title']
            cleanContent = self.mastodon.removeTags(post['content'])
            cleanContent = self.mastodon.htmlEntitiesToString(cleanContent)
            if not self.db.is_published(post['id']) and self.mastodon.shouldPost(cleanContent):
                cleanContent = self.mastodon.handleQuotes(cleanContent, title)
                #print 'Published: ' + title
                #print 'Published: ' + cleanContent
                hashtags = self.find_hashtags(cleanContent, self.keywords)
                if self.hashtags is not None:
                    hashtags.extend(self.hashtags)
                try:
                    self.diasp.post(text=cleanContent, title=None, hashtags=hashtags, source=None, append=self.append)
                    self.db.mark_as_posted(post['id'])
                except Exception as e:
                    print 'Failed to publish: ' + str(e)
