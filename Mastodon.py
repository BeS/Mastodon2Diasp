#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Mastodon.py: check Mastodon feed and format it for Diaspora
#
# Copyright (C) 2017  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
try:
    # Python 2.6-2.7 
    from HTMLParser import HTMLParser
except ImportError:
    # Python 3
    from html.parser import HTMLParser

class Mastodon:

    def __init__(self, mastodonUserName):
        self.mastodonUserName = mastodonUserName

    def shouldPost(self, text):
        """ check if post starts with a '@'

        :param text: message which should be posted to Diaspora
        :type text: str
        :return: true if the message should be bosted
        :rtype: bool
        """
        if text.startswith('@'):
            return False
        return True

    def removeTags(self, text):
        """ remove html tags from text

        :param text: str
        :rtype: str
        """
        TAG_RE = re.compile(r'<[^>]+>')
        cleanText = TAG_RE.sub('', text)
        return cleanText.strip()

    def htmlEntitiesToString(self, text):
        """ convert html entities to strings

        :param text: str
        :rtype: str
        """
        h = HTMLParser()
        return h.unescape(text)

    def handleQuotes(self, text, title):
        """ add a 'via xyz' in case of a quote
        :param text: str
        :param title: str
        :rtype: str
        """
        newText = text
        searchString = self.mastodonUserName + ' shared a status by'
        if title.startswith(searchString):
            wordList = title.split()
            newText = newText + ' via ' + wordList[-1]
        return newText
